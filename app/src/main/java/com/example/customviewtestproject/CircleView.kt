package com.example.customviewtestproject

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import kotlin.math.min

@SuppressLint("ResourceAsColor")
class CircleView(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
    View(context, attrs, defStyleAttr, defStyleRes) {

    constructor (context: Context, attributesSet: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attributesSet,
        defStyleAttr,
        0
    )

    constructor (context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor (context: Context) : this(context, null)

    private var paint = Paint()
    private var circleSize: Float = 100f
    private var startColor: Int = 0
    private var endColor: Int = 0
    private var gradient: LinearGradient? = null
    private var colors = intArrayOf()
    private var blurMaskFilter: BlurMaskFilter =
        BlurMaskFilter(circleSize / 2, BlurMaskFilter.Blur.NORMAL)

    init {
        val typedArray: TypedArray = context.theme.obtainStyledAttributes(

            attrs,
            R.styleable.CircleView,
            0,
            0
        )
        val radius = typedArray.getDimension(R.styleable.CircleView_radius, 0f)
        circleSize = typedArray.getDimension(R.styleable.CircleView_circleSize, 0f)

        if (radius != 0f) {
            blurMaskFilter = BlurMaskFilter(radius, BlurMaskFilter.Blur.NORMAL)
        }

        startColor = if (startColor != 0) {
            typedArray.getColor(R.styleable.CircleView_startColor, 0)
        } else {
            ContextCompat.getColor(context, R.color.startDef)
        }

        endColor = if (endColor != 0) {
            typedArray.getColor(R.styleable.CircleView_endColor, 0)
        } else {
            ContextCompat.getColor(context, R.color.endDef)
        }

        colors = intArrayOf(startColor, endColor)
        typedArray.recycle()
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val centerX = width / 2
        val centerY = height / 2
        val radius = (min(centerX, centerY) + circleSize)

        val positions = floatArrayOf(0f, 1f)
        gradient =
            LinearGradient(
                0f,
                0f,
                0f,
                height.toFloat(),
                colors, positions,
                Shader.TileMode.CLAMP
            )

        paint.apply {
            colorFilter = null
            shader = gradient
            maskFilter = blurMaskFilter
        }
        canvas.drawCircle(centerX.toFloat(), centerY.toFloat(), radius, paint)
    }
}