package com.example.customviewtestproject

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.example.customviewtestproject.databinding.IconBinding

class IconView(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int,
    desStyleRes: Int
) : FrameLayout(context, attrs, defStyleAttr, desStyleRes) {
    private val binding: IconBinding

    constructor (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor (
        context: Context,
        attrs: AttributeSet?
    ) : this(context, attrs, 0)

    constructor (
        context: Context
    ) : this(context, null)

    init {
        binding = IconBinding.inflate(LayoutInflater.from(context), this)

        val typedArray: TypedArray = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.IconView,
            0,
            0
        )

        val colorIcon = typedArray.getColor(R.styleable.IconView_color, 0)
        val colorCircle = typedArray.getColor(R.styleable.IconView_circleColor, 0)

        binding.icon.setColorFilter(colorIcon)
        binding.backgroundCircle.setColorFilter(colorCircle)

        typedArray.recycle()
    }
}