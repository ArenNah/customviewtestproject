package com.example.customviewtestproject

import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import androidx.appcompat.app.AppCompatActivity
import com.example.customviewtestproject.databinding.ActivityMainBinding

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val spannableString = SpannableStringBuilder(resources.getString(R.string.info))
        spannableString.setSpan(StyleSpan(Typeface.BOLD), 0, 24, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.into_text_view_color_orange)),
            69,
            88,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        binding.textView.text = spannableString
    }
}